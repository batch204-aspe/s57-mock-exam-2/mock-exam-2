let collection = [];

// Write the queue functions below.

// USAGE of JS ARRAY methods (push, pop, shift, unshif, etc) is not Allowed

// Properties (like .lenght) are allowed

// Print all the elements
function print() {
    
     return collection;
}

// add elements in the last element/index
function enqueue(element) {

    collection[collection.length] = element;
    return collection;
}

// remove the 1st element/index[0]
function dequeue() {
    for (let i = 0; i < collection.length - 1; i++) {
        collection[i] = collection[i + 1]
    }
    collection.length --
    return collection;

    /* Other Solution

        let newCollection = [];

            for(let i = 1; i < collection.length; i++){
                newCollection[i-1] = collection[i]
            }

            collection = newCollection;
            return collection;

    */
}

// Access the 1st element/index[0]
function front() {
    return collection[0];
}

// show total number of elements
function size() {
    return collection.length;
}

// check if the array is EMpty
function isEmpty() {

    if(collection.length === 0) {
        return true;
    } else {
        return false
    }

    /* Other solution
        Much Shorter 
    
        return collection === 0;

    */

}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};